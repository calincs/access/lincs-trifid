FROM zazuko/trifid:2.3.7

COPY ./config-stage.json /app/config-stage.json
COPY ./config-prod.json /app/config-prod.json
COPY ./config-custom.json /app/config-custom.json
COPY ./logo.png /app/node_modules/trifid-renderer-simple/public/logo.png
COPY ./views /app/views/